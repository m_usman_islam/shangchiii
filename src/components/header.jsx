import Dragon from '../images/dragon.svg';
import Chinese from '../images/chinese.svg';
import Logo from '../images/logo.svg';
import Bg1 from '../images/bg1.jpg';
import Bg2 from '../images/bg2.jfif';
import Heroe from '../images/cast/hero.jpg';
import Heroine from '../images/cast/heroine.jpg';
import Villan from '../images/cast/villan.jpg';
import Female from '../images/cast/female-villan.jpeg';
import { useState, useEffect, useRef } from 'react';
import { gsap, Power3 } from 'gsap';
const Header = () => {
  let movieName = useRef(null);
  let movieTag = useRef(null);
  let castss = useRef(null);
  let header = useRef(null);

  let q = gsap.utils.selector(header);
  const [casts] = useState([
    { name: 'Simu Liu', img: Heroe },
    { name: 'Awkwafina', img: Heroine },
    { name: 'Tony Leung ', img: Villan },
    { name: 'Fala Chen', img: Female },
  ]);

  useEffect(() => {
    gsap.to(header.current, { delay: 0.5, visibility: 'visible' });

    gsap.fromTo(
      [movieName, movieTag],

      { delay: 0.5, y: 140, opacity: 0 },
      {
        y: 0,
        opacity: 1,
        duration: 1,
        ease: Power3.easeIn,
        stagger: 0.4,
      }
    );

    gsap.fromTo(
      q('.up'),
      { y: 60, opacity: 0 },
      { y: 0, opacity: 1, duration: 1, ease: Power3.easeIn, stagger: 0.5 }
    );
    gsap.fromTo(
      q('.cast-item'),
      { y: 60, opacity: 0 },
      { y: 0, opacity: 1, duration: 1, ease: Power3.easeIn, stagger: 0.5 }
    );
    gsap.fromTo(
      q('.bgbox'),
      { y: '100%', opacity: 0 },
      { y: 0, opacity: 1, duration: 1, ease: Power3.easeIn, stagger: 0.5 }
    );
    gsap.fromTo(
      q('.bg-img'),
      { delay: 5, scale: 1.5 },
      { scale: 1, duration: 3, ease: Power3.ease, stagger: 1 }
    );
  }, [q]);

  return (
    <header className="header" ref={header}>
      <img src={Chinese} alt={Chinese} className="bg  chinese" />
      <img src={Dragon} alt={Dragon} className="bg  dragon" />

      <div className="header-content">
        <div className="header-content-inner">
          <div className="content-left">
            <img src={Logo} alt={Logo} className="logo" />
            <div className="content-left-inner">
              <span className="tag">Movie</span>
              <h1 className="movie-name">
                <span className="upslide" ref={(el) => (movieName = el)}>
                  SHANG CHI <span className="rotet">AND</span>
                </span>
              </h1>
              <h2 className="movie-tag">
                <span ref={(el) => (movieTag = el)}>
                  THE LEGEND OF TEN RIGNS
                </span>
              </h2>
              <div className="movie-info">
                <span className="tag-info up">Produced by: </span>
                <span className="data up">Kevin Feige , Jonathan Schwartz</span>
              </div>
              <div className="movie-info">
                <span className="tag-info up">Box office: </span>
                <span className="data up">$423.7 million</span>
              </div>
            </div>
            <div className="casts" ref={castss}>
              {casts.map((cast) => {
                return (
                  <div className="cast-item" key={cast.name}>
                    <span className="read-more">Read More</span>
                    <div className="cast-img">
                      <img src={cast.img} alt={cast.img} />
                    </div>
                    <span className="cast-name">{cast.name}</span>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="content-right">
            <div className="bg1Box bgbox">
              <img src={Bg1} alt={Bg1} className="bg1 bg-img" />
            </div>
            <div className="bg2Box bgbox">
              <img src={Bg2} alt={Bg2} className="bg2 bg-img" />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
