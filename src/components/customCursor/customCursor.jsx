import './customCursor.css';
import { useEffect, useRef } from 'react';
const CustomCursor = () => {
  let cursor = useRef(null);
  useEffect(() => {
    document.addEventListener('mousemove', (e) => {
      let { clientX, clientY } = e;
      // formula
      let mouseX = clientX - cursor.clientWidth;
      let mouseY = clientY - cursor.clientHeight;

      cursor.style.transform = `translate3d(${mouseX}px,${mouseY}px,0)`;
    });
  }, []);
  return <div id="app-custom-cursor" ref={(el) => (cursor = el)}></div>;
};

export default CustomCursor;
