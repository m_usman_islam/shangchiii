import './styles/App.css';
import Header from './components/header';
import CustomCursor from './components/customCursor/customCursor';
const App = () => {
  return (
    <div id="App">
      <CustomCursor />
      <Header />
    </div>
  );
};

export default App;
